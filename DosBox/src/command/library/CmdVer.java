package command.library;

import interfaces.IDrive;
import interfaces.IOutputter;
import command.framework.Command;

public class CmdVer extends Command {

	protected CmdVer(String commandName, IDrive drive) {
		super(commandName, drive);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(IOutputter outputter) {
		outputter.printLine("Version 1");
	}

}
